package com.study;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication //标注该类为主程序启动类
public class Main {
    //主程序启动方法
    public static void main(String[] args) {
//        System.out.println("Hello world!");
        SpringApplication.run(Main.class,args);
    }
}